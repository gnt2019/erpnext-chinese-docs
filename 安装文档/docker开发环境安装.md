安装开发环境，原文档参考 https://github.com/frappe/frappe_docker/tree/develop/development

 **前置条件** 

- 安装docker
ubuntu下命令（参考https://docs.docker.com/compose/install/）

```
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
sudo apt-get update  
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

- 安装docker-compose

```
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

- 将当前用户添加到docker用户组
将当前用户添加到 docker 组

```
sudo gpasswd -a ${USER} docker

```

重新登录或者用以下命令切换到docker组
`newgrp - docker`

重启docker服务
`sudo service docker restart`

验证不加sudo直接执行docker命令检查效果
`docker images`

 **安装** 

1. 下载安装项目文件到本地安装目录
`git clone https://github.com/frappe/frappe_docker.git`

2. 切换到安装目录
`cd frappe_docker`

3. 复制样板开发配置参数文件
`cp -R devcontainer-example .devcontainer`

4. 启动开发环境docker虚拟机
`docker-compose -f .devcontainer/docker-compose.yml up -d`

5. 连接进入docker虚拟机环境命令终端
`docker exec -e "TERM=xterm-256color" -w /workspace/development -it devcontainer_frappe_1 bash`

6. 在虚拟机中安装ERPNext


- 6.1 bench初始化（安装Frappe框架），生成frappe-bench目录
`bench init --skip-redis-config-generation --frappe-branch version-13-beta frappe-bench`

 _如果出现[Errno 13] Permission denied: 'frappe-bench'
ERROR: There was a problem while creating frappe-bench错误，
请执行命令_  `sudo chown -R frappe .`

- 6.2 切换到frappe-bench目录
 `cd frappe-bench`

- 6.3. 设置关联服务（数据库mariadb和缓存数据库redis)，即容器服务名,非docker模式下默认为本机localhost下的其它服务进程

```
bench set-mariadb-host mariadb
bench set-redis-cache-host redis-cache:6379
bench set-redis-queue-host redis-queue:6379
bench set-redis-socketio-host redis-socketio:6379
```
- 6.4 删除Honcho进程清单配置文件Procfile中的redis进程条目，原因是非docker模式下，Honcho进程管理机制自动启动本机localhost的redis进程,
docker模式下redis服务是由docker引擎（docker-compose中定义的redis服务）启动的(如出错提示说此文件不存在，忽略即可）
 `sed -i '/redis/d' ./Procfile`

 
- 6.5 创建新站点（数据库），请替换以下命令中的数据库和管理员初次登录系统的密码，该步骤完成后会将新站点设为默认站点，后续bench命令可不带--site参数
 `bench new-site mysite.com --mariadb-root-password 123 --admin-password admin --no-mariadb-socket`

- 6.6 开启开发者模式，清缓存

 `bench set-config developer_mode 1`

 `bench clear-cache` 
 
- 6.7 安装ERPNext
- - 6.7.1 从代码托管网站（类似手机应用商店）下载ERPNext应用并作为一个包安装到python环境中，如果自己fork的项目在gitee上，也
可替换为gitee网址
    `bench get-app --branch version-13-beta erpnext https://github.com/frappe/erpnext.git`

- - 6.7.2 将ERPNext包安装到指定站点（数据库）
 `bench install-app erpnext`
 
- - 6.7.3 启动ERPNext
 `bench start`
 
- 6.8 （可选)安装自定义或第三方应用，如本项目的中文本地化应用
- - 6.8.1 下载代码并安装到python环境中
 `bench get-app ebclocal https://gitee.com/yuzelin/ebclocal.git`

- - 6.8.2 安装到站点
 `bench install-app ebclocal`

 
- 7. 使用用户电脑浏览器访问系统
 宿主机名或IP:8000


常见错误
- bench start 失败

- - Error: ENOSPC: System limit for number of file watchers reached, watch '/workspace/development/frappe-bench/apps/frappe/frappe/public/less/variables.less'
- - https://stackoverflow.com/questions/55763428/react-native-error-enospc-system-limit-for-number-of-file-watchers-reached
- - 原因：原因是开发者模式下，系统自动侦测代码文件的变更，宿主机默认配置的允许监控最大多少文件变更数字太小
- - 解决方案：在宿主机上执行以下命令，容器与宿主机共享此配置信息，不能在dockerfile或容器里执行此命令
- - echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
- - 验证执行成功与否：应该返回524288
- - cat /proc/sys/fs/inotify/max_user_watches  

                                             

- 网页初始化设置失败


- - 原因是开发者模式下，系统自动侦测代码文件的变更，一有变更自动加载新代码并重启应用，初次登录使用，系统会将python文件
- - 编译为pyc文件，这些编译动作导致了大量的系统自动重启应用。
- - docker exec -it devcontainer_frappe_1 bash  #进入开发环境docker容器中
- - bench update
- - bench start

docker-compose up 命令失败
ERROR: Version in "./.devcontainer/docker-compose.yml" is unsupported.
原因：高版本docker-compose与docker-compose.yml文件不匹配
解决方案：将docker-compose.yml文件中的第二行version="3.7" 中的3.7 改为 3

还可尝试其它建议，如 bench --site reinstall --yes
网页登录重试，有时候多重复几次